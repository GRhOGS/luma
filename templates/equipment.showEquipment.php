<a class="btn ontable" href="equipmentList">Equipmentlisten einsehen</a>

<?php
  //print error/success message for adding new equipment
  if(isset($this->_['addSuccess'])){
    if($this->_['addSuccess']){
      echo'<p class="ontable" style="color: var(--einf);">Das Equipment wurde erfolgreich hinzugefügt!</p>';
    } else {
      echo'<p class="ontable" style="color:var(--del)">Bitte fülle alle Eingabefelder aus!</p>';
    }
  }

  $perms=$this->permissions;
?>

<!-- Tabellenkopf -->
<table><tr><th> Name </th><th> Gesamtanzahl </th><th> Kategorie: <form method="get" action="?" class="pfeil"><select name="category" onchange="this.form.submit()" >
<?php View::printOptions(array_merge(['alle'],$this->categories), $_GET['category']);?>
</select></form> </th><th> Besitzer </th>
            <?php echo $perms>=2?'<th></th>':null; ?></tr>

<?php
  //add new dataset
  if($perms>=2){
    echo'<tr id="Eingabe"><form action="?" method="post">
        <td><input placeholder="Eqipment Name" class="input" type="text" max="64" name="name" value="'.
          (isset($_POST['name'])?$_POST['name']:'').'"></td>
        <td><input id="numb" placeholder="Anzahl" type="number" maxlength="3" name="count" size="3" value="'.
          (isset($_POST['count'])?$_POST['count']:'').'"></td>
        <td class="pfeil" id="p1"><select name="category">';
    if(isset($_GET['category'] )&& $_GET['category']!='alle'){
      View::printOptions($this->categories, $_GET['category']);
    }else{
      View::printOptions($this->categories);
    }

    echo'</select></td>
      <td class="pfeil"><select name="username">';
    View::printOptions($this->_['usernames'], $_SESSION['username']);
    echo '</select></td>
      <td><input class="btn btneinf" type="submit" name="newEntry" value=" Eintrag einfügen "></td>
    </form></tr>';
  }

  // list all equipment
  $equipment=$this->_['equipment'];
  foreach($equipment as $row){
    echo"<tr><td>".$row['equipmentName']."</td><td>".$row['total']."</td><td>".$row['category']."</td><td>";
    echo $row['name'][0].'('.$row['username'][0].'): '.$row['count'][0];
    for($i=1;$i<count($row['name']);$i++){
      echo '<br>'.$row['name'][$i].'('.$row['username'][$i].'): '.$row['count'][$i];
    }
    echo"</td>";
    echo $perms>=2?"<td><form action='?' method='post'><input type='hidden' name ='id' value='".$row['id'].
                    "'><input class='btn' type='submit' name='editEquipment' value=' Eintrag bearbeiten '>
                    </form></td>":null;
    echo"</tr>";
  }
?>
</table>
