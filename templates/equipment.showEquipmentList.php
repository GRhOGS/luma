<a class="btn ontable" href="equipmentList">Equipmentlisten einsehen</a>

<?php
$list=$this->_['list'];

//edit buttons
if($this->permissions>=2){
  echo'<form action="?" method="post"><input type="hidden" name="id" value="'.$this->_['listID'].'">
    <input type="hidden" name="listName" value="'.$list['listName'].'">
    <input class="btn utable btneinf" type="submit" name="editList" value=" Equipment der Liste hinzufügen/der Liste entfernen ">';
?>
    <input class="btn utable btndel" type="submit" name="deleteList" value=" Liste löschen "
          onclick="return confirm('Sicher, dass du die Equipmentliste löschen möchtest?');">
<?php
    echo '</form>';
}

  //if table not empty
  if(isset($list['listID'])){
    $equipment=$list['equipment'];

    //Header w/ list name
    echo'<table><tr><th id="name" colspan="4">'.$list["listName"].'</th></tr>';
    echo'<tr><th>Equipmentname</th><th>benötigt/verfügbar</th><th>Kategorie</th><th>Besitzer</th></tr>';

    //print out every equipment element
    foreach($equipment as $row){
      $equip=$row['equipment'][$row['id']];
      echo"<tr><td>".$equip['equipmentName']."</td><td>";
      //warn if needed>total
      echo $row['needed']>$equip['total']?'<p style="color:var(--del)">':'<p>';

      echo $row['needed']."/".$equip['total']."</p></td><td>".$equip['category']."</td><td>";
      for($i=0;$i<count($equip['name']);$i++){
        echo $equip['name'][$i]."(".$equip['username'][$i]."): ".$equip['count'][$i]."<br>";
      }
    }
    echo"</td></tr></table>";
  }else{
    echo"<p id='empty'>Die Liste ist leer :(</p>";
  }

?>
