<a class="btn ontable" href="equipmentList">Equipmentlisten einsehen</a>

<form action="?" method="post">
<?php
  $this->permissions<2?Controller::loginError():null;
  $equipment=$this->_['equipment'];
  $selected=$this->_['selected'];

  //Header w/ list name
  echo'<table><tr><th id="name" colspan="3">'.$this->_['listName'].'</th></tr>';
  echo"<tr><th>Equipmentname</th><th>benötigt/gelistet</th></tr>";

  //print out every equipment element
  foreach($equipment as $key=>$row){
    //prefill amount
    $amount=in_array($key, array_keys($selected))?$selected[$key]:0;

    //print out row
    echo"<tr><td>".$row['equipmentName']."</td>";
    echo'<td><input class="numb" type="number" name="'.$key.'" value="'.$amount.'" min="0"><p id="Aftertext"> von '.$row["total"].'</p></td></tr>';
  }
?>
</table>
<input type="hidden" name="id" value="<?php echo $this->_['listID'] ?>">
<input class="btn" id="Weardo" type="submit" name="updateList" value=" Liste aktualisieren"></form>
