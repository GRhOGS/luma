<!-- video section -->

<div id="angebote-sec-1">
<input type="checkbox" id="angebot-1-toggler">
<section id="angebote-background-1">
	<div class="angebote-mid-right" >
		<div class="angebote-placeholder"></div>
		<div class="angebote-content">
			<div class="angebote-text">
				<h1>Videoproduktion</h1><p>Das Erzählen einer Geschichte ist die Essenz eines Videos. Du hast eine? Wir helfen Dir sie umzusetzen! Oder brauchst du einfach ein Video für einen neuen Track oder als Vorschau für deine Firma?<a href="angebote#Contact"> Kontaktiere uns!</a></p>
			</div>
			<div class="angebote-btn">
				<a href="videos" class="btn">Schau dir unsere Videos an!</a>
			</div>
		</div>
	</div>
</section>
	<div class="angebote-kreuz">
		<div><div></div></div>
	</div>
</div>

<!-- music section -->

<div id="angebote-sec-2">
	<input type="checkbox" id="angebot-2-toggler"></input>
	<section id="angebote-background-2">
		<div class="angebote-mid-left">
			<div class="angebote-content">
				<div class="angebote-text">
					<h1>Musikproduktion</h1><p>Der Text stimmt, aber der Aufnahme fehlt noch die Würze… Wir nehmen deine Musik auf und Produzieren sie um mit Dir den perfekten Geschmack zu erlangen.<a href="angebote#Contact"> Kontaktiere uns!</a></p>
				</div>
				<div class="angebote-btn">
					<a href="music" class="btn">Schau dir unsere Musik an!</a>
				</div>
			</div>
			<div class="angebote-placeholder"></div>
		</div>
	</section>
	<div class="angebote-kreuz">
		<div><div></div></div>
	</div>
</div>

<!-- photo section -->

<div id="angebote-sec-3">
	<input type="checkbox" id="angebot-3-toggler">
	<section id="angebote-background-3">
		<div class="angebote-mid-right">
			<div class="angebote-placeholder"></div>
			<div class="angebote-content">
				<div class="angebote-text">
				<h1>Fotografie</h1><p>Jede Seite ist eine Schokoladenseite! Unsere Fotografen stellen Portraits und Landschaft in ein anderes Licht! Jedes Event ist mit uns ewig am leben.<a href="angebote#Contact"> Kontaktiere uns!</a></p>
				</div>
				<div class="angebote-btn">
					<a class="btn" href="fotos">Schau dir unsere Fotos an!</a>
				</div>
			</div>
		</div>
	</section>
	<div class="angebote-kreuz">
	<div><div></div></div>
	</div>
</div>

<!-- contact form -->

<section id="Contact">
	<div id="Contactdiv">
		<!-- form -->
		<form class="" action="angebote#Contact" method="post">
			<h1>Hier kannst Du uns kontaktieren…</h1>
			<div class="firstrow">
				<input  type="text" name="name" placeholder="Dein Name*">
				<input  type="text" name="email" placeholder="Deine E-Mail Adresse*">
			</div>
			<input class="secondrow" type="text" name="betreff" placeholder="Ihr Anliegen*">
			<textarea name="content" placeholder="Deine Nachricht*"></textarea>
			<div id="bottom">
			<button class="btn" type="submit" name="submit">Absenden</button>
			<?php
				//print error messages

				if(isset($this->_['error']['message'])){
					echo '<p><br>bitte gib eine Nachricht ein!</p>';
				}
				if(isset($this->_['error']['subject'])){
					echo '<p><br>bitte gib einen Betreff ein!</p>';
				}
				if(isset($this->_['error']['name'])){
					echo '<p><br>bitte gib einen Namen ein!</p>';
				}
			?>
		</div>
		</form>



	</div>
</section>

<script type="text/javascript">
//scroll to Contact form if page is visited over 'Kontakt' link
	window.onload = function(){
		 if (location.hash === 'Contact') {
			goto('Contact', this);
		 }
		window.scrollBy(0,-200);
	}
</script>
