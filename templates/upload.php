<?php
//dont show error messages (can't jump to deleted post after reload)
error_reporting(E_ERROR | E_PARSE);

/* -------- select media type --------- */
if(!isset($this->_['type'])){
  echo '<h1 id="Upload-Title">Welche Medien möchtest du bearbeiten oder hochladen?</h1>';
  echo '<form action="?" method="GET">
          <div id="Select-Buttons">
            <button id="Audio-btn" type="submit" name="type" value="audio">
              ';
              SVG::headphones();
              echo'
                <p>Audio</p>
              </button>
            <button id="Bild-btn" type="submit" name="type" value="photo">
              ';
              SVG::photo();
              echo'
              <p>Foto</p>
              </button>
            <button id="Video-btn" type="submit" name="type" value="video">
              ';
                SVG::video();
              echo'
             <p>Video</p </button>
          </div>
        </form>';
}else{

  /* -------- upload new media ---------- */
  $type=$this->_['type'];

  //categories
  $audioCats=['Live Sessions', 'Rock', 'Filmmusik', 'Hip Hop'];
  $videoCats=['Musikvideos', 'Sketches', 'Tutorials', "Making Of's", 'Reviews'];
  $photoCats=['Landschaft', 'Stadt', 'Tiere', 'Portraits', 'Events', 'Drehs'];

  if($type=='video'){
    $categories=$videoCats;
  }else if($type=='audio'){
    $categories=$audioCats;
  }else if($type=='photo'){
    $categories=$photoCats;
  }


  echo '<h1 style="padding:10vh 0 0 5vw;">Uploade dein ';
    if($type=='video'){
      echo 'Video ';
    }else if($type=='photo'){
      echo 'Bild ';
    }else if($type=='audio'){
      echo 'Musik ';
    }
    echo'</h1><form action="?" method="POST" enctype="multipart/form-data"><div id="Upload-Wrap"><div id="upload-sec">';
    if(isset($_POST['userSelect'])){
      echo '<input type="hidden" name="userSelect" value="'.$_POST['userSelect'].'">';
    }else{
      if($type=='photo'){
        echo'<input type="hidden" name="userSelect" value="deine">';
      }else{
        echo'<input type="hidden" name="userSelect" value="alle">';
      }
    }

  //thumbnail
  if($type!='photo'){
    echo '<div id="drop-tn" class="drop-zone"><span class="drop-zone__prompt">Hier Thumbnail droppen oder klicken zum uploaden <br> (max 500kb)</span> <input type="file" name="thumbnail" class="drop-zone__input"></div>';
  }else{
    echo'<input type="hidden" name="thumbnail" value="photo">';
    echo'<div class="pfeil"><select id="p1" name="creator">';
    View::printOptions($this->_['creators'],$_SESSION['username']);
    echo'</select></div>';
  }


  //media(-source)
  if($type=='video'){
    echo'<textarea class="video-link" name="link" rows="7" cols="35"';
    if(isset($this->_['newMedia']['link'])){
      echo'>'.$this->_['newMedia']['link'].'';
    }else{
      echo' placeholder="youtube.com/watch link oder youtu.be/id link ">';
    }
    echo'</textarea>';
  }else{
    echo'<div id="drop-file" class="drop-zone"><span class="drop-zone__prompt">
    Hier Datei droppen oder klicken zum uploaden';
    echo $type=='photo'?'<br> (max 500kb)':'<br> (max 10mb)';
    echo'</span> <input type="file" name="file" class="drop-zone__input"></div>';
  }


  //title
  echo'</div><div id="descrip-sec"><input id="Uploaded-Title" type="text" name="title"';
  if(isset($this->_['newMedia']['title'])){
    echo' value="'.$this->_['newMedia']['title'].'"';
  }else{
    echo' placeholder="äußerst kreativer Titel'.($type=='photo'?' (optional)':'').'"';
  }
  echo'>';

  //description, price (&#10;->line break)
  if ($type == 'video') {
    echo'<textarea class="beschreibung" name="description" rows="7" cols="100" maxlength="600"';
    if(isset($this->_['newMedia']['description'])){
      echo'>'.$this->_['newMedia']['description'];
    }else{
      echo' placeholder="Beschreibung:&#10;Lorem Ipsum dolor sit amet, blablabla....">';
    }
    echo'</textarea>';
  }

echo'</div></div><div id="cats-sec"><h2>Kategorien (mind. 1 auswählen):</h2>';

  //print all categories
  foreach($categories as $cat){
    $checkChecked=isset($this->_['newMedia'])&&in_array($cat, $this->_['newMedia']['categories'])?'checked':'';
    echo $cat.': <input type="checkbox" name="'.$cat.'" '.$checkChecked.'> ';
  }
  unset($cat);

  //price and submit
  echo'<div>Preis:<input class="numb" type="number" name="price" step="0.01" value="'
        .(isset($this->_['newMedia']['price'])?$this->_['newMedia']['price']:"0.00").'">€</div>
        <input type="hidden" name="type" value="'.$type.'">
        <input class="btn btneinf" type="submit" value="';

  if($type=='video'){
    echo 'Video ';
  }else if($type=='photo'){
    echo 'Bild ';
  }else if($type=='audio'){
    echo 'Musik ';
  }
  echo  'hochladen" name="upload"></form></div>';

  //upload success/failure messages
  if (isset($this->_['uploaded'])) {
    if(!$this->_['uploaded']){
        echo '<script>alert("Upload fehlgeschlagen, warscheinlich wurden nicht alle Felder ausgefüllt!");</script>';
    }else{
      echo '<p style="color: var(--einf); margin-left: 1vw; font-family: calibri;">Datei erfolgreich hochgeladen</p>';
    }
  }

  /* ----------- edit media ---------- */
  echo'<form action="?" method="post"> <input type="hidden" name="type" value="'.$type.'">
      <h1 style="padding:5vh 0 4vh 5vw;">Bearbeite  ';
  if($type=='photo'){
    echo'<div class="pfeil sortdiv"><select id="p1" name="userSelect" onchange="this.form.submit()">';
    //list of all creators
    foreach($this->_['creators'] as $creator){
      if($creator!=$_SESSION['username']){
        $creators[]=$creator."'s";
      }else{
        $creators[]='deine';
      }
    }
    View::printOptions(array_merge(['alle', 'deine'],$creators),
                      isset($_POST['userSelect'])?$_POST['userSelect']:'deine');
    echo"</select></div>";
  }else{
    echo 'die';
  }
  if($type=='video'){
    echo ' Videos ';
  }else if($type=='photo'){
    echo ' Bilder ';
  }else if($type=='audio'){
    echo ' Audios ';
  }
  echo'</h1></form><table class="table"><tr><th></th><th>Thumbnail</th><th class="titel-td">Titel</th>';
  if($type=='photo'){echo'<th>Ersteller</th>';}
  if($type=='video'){echo'<th >Beschreibung</th>';}
  echo'<th>Genre</th><th>Preis</th><th></th></tr>';
  $path='media/thumbnails/';

  //print all media
  foreach($this->_['media'] as $id=>$media){
    //delete
    echo'<tr><form action="upload'.(isset($id)?"#$id":null).'" method="POST"><td>
        <input type="hidden" name="type" value="'.$type.'">';
    if(isset($_POST['userSelect'])){
      echo '<input type="hidden" name="userSelect" value="'.$_POST['userSelect'].'">';
    }else{
      if($type=='photo'){
        echo'<input type="hidden" name="userSelect" value="deine">';
      }else{
        echo'<input type="hidden" name="userSelect" value="alle">';
      }
    }
    ?>
    <!-- delete button -->
    <button class="btn btndel" type="submit" name="deleteMedia" value="<?php echo $id; ?>"
            onclick="return confirm('Sicher, dass du den Eintrag löschen möchtest?');">
    Löschen</button></td><td width="200px;">
    <?php
    //media preview
    if($type=='photo'){
      $src=$media['link'];
    }else{
      $src=$path.$id.View::getExtension($id, $path);
    }
    echo'<div class="prev-thumb"><img id="'.$id.'" src="'.$src.'"><div></td>';
    //titel
    echo'<td><input class="input" type="text" name="title" value="'.$media['titel'].'"></td>';
    //creator
    if ($type=='photo') {
      if(isset($media['creator'])){
        echo'<td class="pfeil"><select name="creator">';
        View::printOptions($this->_['creators'], $media['creator']);
        echo'</select></td>';
      }
    }

    //description
    if($type=='video'){
    echo'<td><textarea name="description" class="besch-td" rows="7" cols="100" maxlength="600">'.
          $media['beschreibung'].'</textarea></td>';}
    //genre
    echo'<td style="min-width:150px;">';
    foreach($categories as $cat){
      (in_array($cat, $media['categories'])?$checkChecked='checked':$checkChecked='');
      echo $cat.': <input type="checkbox" name="'.$cat.'" '.$checkChecked.'><br> ';
    }
    //price
    echo'</td><td><p>Preis:<input class="numb" type="number" name="price" step="0.01" value="'
          .$media['preis'].'">€</p></td>';
    //update button
    echo'<td><button class="btn" type="submit" name="updateMedia" value="'.$id.'">Änderungen übernehmen</button></form></tr>';
  }
  echo"</table> </div>";
  // upload success

}
 ?>

 <script type="text/javascript" src="js/dragndrop.js"></script>
 <script type="text/javascript" src="js/lottie.js"></script>
 <script type="text/javascript" src="js/upload.js"></script>
 <script type="text/javascript">
 window.onload = function(){
   //if media is edited, scroll to edited media
    if (location.hash === <?php echo '"' . $id . '"';?>) {
 		 goto(<?php echo "'" . $id . "'";?>, this);
    }
 	 window.scrollBy(0,-200);
 }

</script>
