<div id="Interval-Res">
		<?php
			$members = $this->_["team"];
			$members[]=["username"=>'', "vorname"=>'',"name"=>'',"email"=>'', "newMember"=>true,
									"youtubelink"=>'',"instalink"=>'',"pos"=>(count($members)+1),"funktion"=>[],
									"beschreibung"=>'...ist [role] bei LUMA PRODUCTIONS'];
			//print out every member
			foreach($members as $member) {
				echo '<div class="team-mitglied"';
				echo isset($member['newMember'])?'id="new-member"':null;
				echo '>';
				if(isset($member['newMember'])){
					echo '<img src="pics/Profile-Pictures/new.png" alt="Image not found">';
				}else{
					echo '<img src="pics/Profile-Pictures/'.$member['username'].'.jpg" alt="Image not found">';
				}
				echo '<form action="?" method="POST" enctype="multipart/form-data">
							<div class="right">
								<p>Username: <input type="text" name="username" value="'.$member['username'].'" size="8"></p>';
				echo isset($member['newMember'])?'<p>Passwort: <input type="text" name="pass" size="8"></p>':null;
				echo '	<p>Position: <input type="number" name="pos" value="'.$member['pos'].'" size="3" min="1"></p>
								<p>Vor-, Nachname: <span style="display:inline"><input type="text" name="vorname" value="'.$member['vorname'].'"size="9">
																		<input type="text" name="name" value="'.$member['name'].'"size="9"></span></p>
								<p>E-Mail: <input type="text" name="email" value="'.$member['email'].'" size="30"></p>
								<br><p>Beschreibungstext:</p><textarea name="beschreibung" rows="4" cols="42">'.$member['beschreibung'].'</textarea>
								<p>Youtube: <input type="text" name="youtubelink" value="'.$member['youtubelink'].'" size="39" onClick="this.select();"></p>
								<p>Insta: <input type="text" name="instalink" value="'.$member['instalink'].'" size="39" onClick="this.select();"></p>';
				//all given roles
				if(isset($member['newMember'])){
					foreach($this->_['roles'] as $role => $perms){
						echo '<input type="checkbox" name="'.$role.'" id="'.$role.'-'.$member['username'].'" value="checkedBox">
									<label for="'.$role.'-'.$member['username'].'" class="nobr">'.$role.' ['.$perms.']</label>';
					}
				}else{
					foreach($member['funktion'] as $role){
						echo '<input type="checkbox" name="'.$role.'" id="'.$role.'-'.$member['username'].'" checked value="checkedBox">
									<label for="'.$role.'-'.$member['username'].'" class="nobr">'.$role.' ['.$this->_['roles'][$role].']</label>';
					}
				}

				if(isset($member['newMember'])){
					echo '<p>Hintergrundbild: <input type="file" name="headshot"></p>';
					echo '<div><button type="submit" name="newMember"> Wilkommen im Team! </button></div></form>';
				}else{
					echo '<div><button type="submit" name="editMember" value="'.$member['username'].'">akualisieren</button>';
					echo '<button id="delButton" type="submit" name="deleteMember" value="'.$member['username'].'">löschen</button></div></form>';
					$newRoles=[];
					foreach($this->_['roles'] as $role=>$perms){
						if(!in_array($role, $member['funktion'])){
							$newRoles[$role]=$perms;
						}
					}
					echo'<form action="?" method="POST">
								<input type="hidden" name="username" value="'.$member['username'].'">
								<select class="dropdown" name="addRole" onchange="this.form.submit()">
									<option value="default" style="text-align:center">--- neue Rolle zuweisen ---</option>';
									foreach($newRoles as $role => $perms){
										echo '<option value="'.$role.'">'.$role.' ['.$perms.']</option>';
									}
					echo'</select></form>';
				}
				echo '</div></div>';
			}
		?>
</div>
