<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="parallax-container top" style="height: 100vh; width: 100vw; ">
	<div class="parallax-item 20">
		<img id="Main-Pic" src="pics/Start-Frame/music-HG.jpg" style="height: 100vh; width: 100vw; object-fit: cover;">
	</div>

	<div class="parallax-item 40">
		<h1 id="Musik-Titel">Musik</h1>
	</div>

	<div class="parallax-item 70">
		<img id="Main-Pic" src="pics/Start-Frame/music-VG.png" style="height: 100vh; width: 100vw; object-fit: cover;">
	</div>
</div>

<!-- content -->
<div class="interval-backdrop" id="Interval-Res-Back"></div>
<div class="interval" id="Interval-Res">
	<section id="show">
		<div id="Audio-Show">
			<!-- get selected song -->
			<?php
				$music = $this->_['music'];
				$suggestFav="1";
				$path="media/thumbnails/";;

				if(in_array($suggestFav, array_keys($music))){
					$suggest=$suggestFav;
				}else{
					$suggest=array_keys($music)[0];
				}

				if(isset($_GET['setmusic'])){
					$suggest=$_GET['setmusic'];
				}
			?>

			<!-- show selected music -->
			<div id="Audio-Box">
				<div id="Selected-Audio-Pic">
					<img src="<?php echo $path.$suggest.View::getExtension($suggest, $path); ?>" alt="">
				</div>
				<div id="Selected-Audio">
					<p id="Titel"><?php echo $music[$suggest]['titel']; ?></p>
					<div id="Audio-Bar">
					<div class="audio-player">
						[
						{
						"src": "<?php echo $music[$suggest]['link']; ?>",
						"type": "audio/mpeg"
						}
						]
					</div>
					<i class="fa fa-volume-up" aria-hidden="true" id="Volume-Icon" >
						<div id="Volume-Back">
							<input id="volumeslider" class="volumeslider" type="range" min="0" max="100" value="80" step="1">
						</div>
					</i>
					</div>
				</div>
			</div>

		</div>

		<div id="Audio-List">
			<!-- sorts -->
			<form id="Selects" action="music#show" method="GET" >
				<div class="haken">
					<select class="music-drop" name="sortMusic" onchange="this.form.submit()">
						<?php View::printOptions($this->_['sortMusic'],$this->_['sortMusicSet']) ?>
					</select>
				</div>
				<div class="haken">
					<select class="music-drop" name="musicCount" onchange="this.form.submit()">
						<?php View::printOptions($this->_['musicCount'],$this->_['musicCountSet']) ?>
					</select>
				</div>
			</form>
			<form action="music#show" method="GET" >
				<table>
					<?php
						foreach($music as $key=>$row) {
							echo '<tr><td><button ';
							if($key==$suggest) echo 'id="Selected-Muc"';
							echo ' class="dev-button" type="submit" name="setmusic" value="'.$key.'" >
										<img id="Audio-Button" src="'.$path.$key.View::getExtension($key, $path).'">
										<i class="fa fa-play sample-play" aria-hidden="true"></i>
										<p class="list-title">'.$row['titel'].'</p><p class="play-but"> auswählen </p>
										</button></td></tr>';
						}
					?>
				</table>
				<input type="hidden" name="sortMusic" value="<?php echo $this->_['sortMusicSet']; ?>">
				<input type="hidden" name="musicCount" value="<?php echo $this->_['musicCountSet']; ?>">
			</form>
		</div>
	</section>
	<script src="js/audioplayer.js" charset="utf-8"></script>
	<script type="text/javascript"></script>
</div>
