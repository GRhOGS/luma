
<?php
  //check permissions
  $this->permissions<2?Controller::loginError():null;

  if($this->_['equipment']==false){
    //echo"<head><meta http-equiv='refresh' content='0; URL=equipment'></head>";
  }
  $equipment=$this->_['equipment'][$this->_['id']];
  //print error/success message for adding new equipment
  if(isset($this->_['addSuccess'])){
    if($this->_['addSuccess']){
      echo'<p style="color:var(--einf)">Änderung erfolgreich!</p>';
    } else {
      echo'<p style="color:var(--del)">Fehler bei der Eingabe!</p>';
    }
  }
?>


<!-- table header -->
<div class="tables">
  <table><tr><th> Name </th><th> Kategorie </th><th></th></tr>

  <!-- add new data set -->
  <tr><form action="?" method="post"><input type="hidden" name="id"
            value="<?php echo $equipment['id']; ?>">
    <td><input class="input" type="text" max="64" name="equipmentName"
                value ="<?php echo $equipment['equipmentName']; ?>"></td>
    <td class="pfeil"><select name="category">
        <?php View::printOptions($this->categories, $equipment['category']); ?>
      </select></td>
    <td><input class="btn" type="submit" name="editEquipment" value=" Equipment ändern ">
      </form></td>
    </tr></table>
</div>

<div class="tables">
  <table><tr><th>Besitzer</th><th> Menge (0=löschen) </th><th></th></tr>

  <?php
    // all usernames, amounts
    $usernames = $this->_['usernames'];
    for($i=0;$i<count($equipment['username']);$i++){
      echo'<tr class="selec"><form action="?" method="post"><td>
            <input type="hidden" name="username" value="'.$equipment['username'][$i].'">';
      echo $equipment['username'][$i];
      echo'</td><td></select><input class="numb" type="number" maxlength="3" name="count" size="3" value="'.$equipment['count'][$i].'"></td>';
      echo'<td><input type="hidden" name="id" value="'.$equipment['id'].'">';
      echo'<input class="btn" type="submit" name="editEquipment" value=" Benutzer aktualisieren "></td></form></tr>';
      unset($usernames[array_search($equipment['username'][$i],$usernames)]);
    }
  ?>

  <tr class="selec" style="background-color: var(--accent);">
  <form action="?" method="post"><td class="pfeil" style="background-color: var(--accent);"><select style="background-color: var(--accent);" name="username">
    <?php View::printOptions($usernames); ?>
  </select></td><td style="background-color: var(--accent);"><input style="background-color: var(--accent);" class="numb" type="number" maxlength="3" placeholder="Anzahl" name="count" size="3"></td>
  <td><input type="hidden" name="id" value="<?php echo $equipment['id'] ?>">
  <input class="btn btneinf" type="submit" name="editEquipment" value=" Benutzer hinzufügen "></td></form>

  </tr></table>
</div>
<form action="?" method="post" style="text-align: center">
  <button id="back" class="btn btndel" type="submit" name="deleteEquip" value="<?php echo $equipment['id'] ?>"
          onclick="return confirm('Sicher, dass du das Equipment löschen möchtest?');">
  Equipment löschen</button>
  <a id="back" class="btn" href="equipment">Zurück zur Equipmenübersicht</a>
</form>
