<?php
	session_start();	//start session for session storage
	include('classes/controller.php');
	include('classes/model.php');
	include('classes/view.php');

	//validate Login
	$permissionsRequired=3;
  require_once('etc/login_check.php');

	$request=array();
	$request['view']='cloud';

	//path
	$request['path']=isset($_GET['path'])?$_GET['path']:'';

	//new file
	if(isset($_POST['upload'])){
		$private=isset($_POST['private'])?1:0;
		$request['newFile']=['private'=>$private, 'path'=>$_GET['path']];
	}

	//new folder
	if(isset($_POST['newFolder'])){
		$request['newFolder']=['path'=>$_GET['path'], 'name'=>$_POST['name']];
	}

	//edit file
	if(isset($_POST['changeFile'])){
		$private=isset($_POST['private'])?'[prv]':'';
		$request['changeFile']=['fullname'	=>	$_POST['fullName'],
														'path'			=>	$_POST['path'],
														'name'			=>	$_POST['name'],
														'owner'			=>	$_POST['owner'],
														'private'		=>	$private,
														'extension'	=>	$_POST['extension']];
	}

	//download file
	if(isset($_POST['downloadFile'])){
		$request['downloadFile']=['fullname'=>$_POST['fullName'], 'shortname'=>$_POST['name']];
	}

	//delete media
	isset($_POST['deleteFile'])?$request['deleteFile']=$_POST['fullName']:null;

	//search for file
	isset($_GET['search'])?$request['search']=$_GET['search']:null;
	
	//get user permissions from login check
	$request['permissions']=$perms;
	$uploadController=new Controller($request);
	echo $uploadController->display();
 ?>
