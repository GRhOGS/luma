const openPopupButtons = document.querySelectorAll('[data-popup-target]')
const closePopupButtons = document.querySelectorAll('[data-popup-close]')
const overlay = document.getElementById('Overlay')

openPopupButtons.forEach(button => {
  button.addEventListener('click', () => {
    const popup = document.querySelector(button.dataset.popupTarget)

    openPopup(popup)

  })
})

function openPopup(popup){
  if (popup == null) return
  popup.classList.add('active')
  overlay.classList.add('active')


}

closePopupButtons.forEach(button => {
  button.addEventListener('click', () => {
    const popup = button.closest('.popup')
    closePopup(popup)
  })
})

function closePopup(popup){
  if (popup == null) return
  popup.classList.remove('active')
  overlay.classList.remove('active')
}
