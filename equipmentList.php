<?php
	session_start();	//start session for session storage
	include('classes/controller.php');
	include('classes/model.php');
	include('classes/view.php');

	//validate Login
	$permissionsRequired=1;
  require_once('etc/login_check.php');

	$request=array();
	$request['view']='equipment.showLists';

	//new list
	if(isset($_POST['name'])){
		$request['newList']=$_POST['name'];
		$request['view']='equipment.showEquipmentList';
	}

	//show certain list
	if(isset($_POST['id'])){
		$request['listID']=$_POST['id'];
		//view list
		if(isset($_POST['viewList'])){
			$request['view']='equipment.showEquipmentList';
		}

		//edit list
		if(isset($_POST['editList'])){
			$request['view']='equipment.editEquipmentList';
			$request['listName']=$_POST['listName'];
		}
		//update list
		if(isset($_POST['updateList'])){
			$request['view']='equipment.showEquipmentList';
			//save all checkboxes/amounts in array
			foreach($_POST as $key=>$row){
				if($key!="id" && $key!="updateList") $listContent[$key]=$row;
			}
			$request['updateList']=$listContent;
		}
		//delete list
		if(isset($_POST['deleteList'])){
			$request['deleteList']=true;
		}
	}

	//get user permissions from login check
	$request['permissions']=$perms;
	$equipmentListController=new Controller($request);
	echo $equipmentListController->display();

 ?>
