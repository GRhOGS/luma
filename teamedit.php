<?php
	session_start();	//start session for session storage
	include('classes/controller.php');
	include('classes/model.php');
	include('classes/view.php');


	$request=array();
	$request['view']='teamedit';

	if(isset($_POST['newMember'])){
		$request['newMember']=['username'=>$_POST['username'], 'pos'=>$_POST['pos'],
														'vorname'=>$_POST['vorname'], 'name'=>$_POST['name'], 'pass'=>$_POST['pass'],
														'beschreibung'=>$_POST['beschreibung'], 'email'=>$_POST['email'],
														'youtubelink'=>$_POST['youtubelink'], 'instalink'=>$_POST['instalink']];
		foreach($_POST as $name => $value){
			if($value=="checkedBox"){
				echo $name;
				$request['newMember']['funktion'][]=str_replace('_', ' ', $name);
			}
		}
	}

	if(isset($_POST['deleteMember'])){
		$request['deleteMember']=$_POST['username'];
	}

	if(isset($_POST['addRole'])){
		$request['addRole']=[$_POST['addRole'], $_POST['username']];
	}

	if(isset($_POST['editMember'])){
		$request['editMember']=['oldNick'=>$_POST['editMember'], 'pos'=>$_POST['pos'],
														'username'=>$_POST['editMember'],'name'=>$_POST['name'],
														'vorname'=>$_POST['vorname'], 'email'=>$_POST['email'],
														'beschreibung'=>$_POST['beschreibung'], 'youtubelink'=>$_POST['youtubelink'],
														'instalink'=>$_POST['instalink']];
	}

	$teamController=new Controller($request);
	echo $teamController->display();

 ?>
