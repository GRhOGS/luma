<?php
	session_start();	//start session for session storage
	include('classes/controller.php');
	include('classes/model.php');
	include('classes/view.php');

	$request=array();
	$request['view']='angebote';

	//check for mail input integrity and output correct error messages
	if (isset($_POST['submit'])) {
		$mailTo = "infoistkrass@gmail.com";
		if ($_POST['name']!='') {
			$headers =  'MIME-Version: 1.0' . "\r\n";
			$headers .= 'From:' . $_POST['content'] ."\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

			if ($_POST['betreff']!='') {
				$subject = $_POST['betreff'];
				if ($_POST['content']!='') {
					$txt = "LUMA E-Mail von ". $_POST['name'] . ".\n\n" . $_POST['content'];
					mail($mailTo, $subject, $txt, $headers);

				//error messages
				}else{
					$request['error']['message']=true;
				}
			}else{
				$request['error']['subject']=true;
			}
		}else{
			$request['error']['name']=true;
		}
	}

	$teamController=new Controller($request);
	echo $teamController->display();
 ?>
