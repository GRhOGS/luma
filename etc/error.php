<html lang="de" dir="ltr">
<head>
	<!-- meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-eqiv="X-UA-Compatible" content="ie=edge">

<!-- Stylesheets -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/menu.css">
<link rel="stylesheet" href="css/style-404.css">

<title>Luma Production</title>
</head>
<body>
  <!-- display error code dynamically -->
  <div id="header-box">
    <h1 > <?php echo isset($_GET['error'])?$_GET['error']:'ERROR'; ?> </h1>
    <p>Es ist ein unerwarteter Fehler aufgetreten :( </p>
    <p>Bitte kontaktiere einen Administrator der Seite :)</p>
  </div>
  <a class="btn" href="../index"> Zurück zur Hauptseite </a>
</body>
</html>
