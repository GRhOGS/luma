<?php
	session_start();	//start session for session storage
	include('classes/controller.php');
	include('classes/model.php');
	include('classes/view.php');

	$request=array();
	$request['view']='music';

	if(isset($_GET['sortMusic']) && $_GET['sortMusic'] != null){
		$request['sortMusic']=$_GET['sortMusic'];
	}else{
		$request['sortMusic']='Neu zuerst';
	}

	if(isset($_GET['musicCount'])){
		$request['musicCount']=$_GET['musicCount'];
	}else{
		$request['musicCount']=8;
	}

	$musicController=new Controller($request);
	echo $musicController->display();

 ?>
