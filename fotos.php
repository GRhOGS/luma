<?php
	session_start();	//start session for session storage
	include('classes/controller.php');
	include('classes/model.php');
	include('classes/view.php');
	include('etc/Loadjpgsize.php');


	$request=array();
	$request['view']='fotos';

	if(isset($_GET['sortFoto']) && $_GET['sortFoto'] != null){

		$request['sortFoto']=$_GET['sortFoto'];
	}else{
		$request['sortFoto']='Neu zuerst';
	}

	if(isset($_GET['sortFotograf'])){
		$request['sortFotograf']=$_GET['sortFotograf'];
	}else{
		$request['sortFotograf']='';
	}

	$fotoController=new Controller($request);
	echo $fotoController->display();


 ?>
