<?php
	session_start();	//start session for session storage
	include('classes/controller.php');
	include('classes/model.php');
	include('classes/view.php');
	include('etc/svg.php');

	//validate Login
	$permissionsRequired=3;
  require_once('etc/login_check.php');

	$request=array();
	$request['view']='upload';

	//set media type
	if(isset($_POST['type'])){
		$request['type']=$_POST['type'];
	}
	if(isset($_GET['type'])){
		$request['type']=$_GET['type'];
	}

	//new media
	if(isset($_POST['upload'])){
		if($request['type']=='video'){
		$request['newMedia']=['type'=>$_POST['type'], 'title'=>$_POST['title'],
													'description'=>$_POST['description'], 'price'=>$_POST['price']];
	}else{
		$request['newMedia']=['type'=>$_POST['type'], 'title'=>$_POST['title'],
													'price'=>$_POST['price']];
	}
	//categories
		$categories=[];
		foreach ($_POST as $key => $value) {
			if($value=='on'){
				$categories[]=str_replace('_',' ',$key);
			}
		}
		$request['newMedia']['categories']=$categories;

		//creator
		if(isset($_POST['creator'])){
			$request['newMedia']['creator']=$_POST['creator'];
		}

		//check for valid files
		//orientiert an: https://www.w3schools.com/php/php_file_upload.asp
		$maxImageSize = 0.5 * 1048576;	//500kb
		$maxAudioSize = 10 * 1048576; //10MB
		$maxThumbnailSize = 0.5 * 1048576; //500kb
		$request['allowUpload']=true;
		if(((isset($_FILES['thumbnail'])&&$_FILES['thumbnail']["tmp_name"]!='')||$_POST['type']=='photo') && (!empty($_FILES['file']["tmp_name"])||isset($_POST['link']))){
			//valid file
			if(isset($_POST['link'])||((getimagesize($_FILES["file"]["tmp_name"])!==false)||$_POST['type']=='audio')
					OR(($_POST['type']=='photo' && $_FILES["file"]["size"] > $maxImageSize)||($_POST['type']=='audio' && $_FILES["file"]["size"] > $maxAudioSize))){
				if(isset($_POST['link'])){
					$request['newMedia']['link']=$_POST['link'];
					$request['file']=false;
				}else{
					$request['file']=true;
				}
			}else{
				echo"<p>Keine Datei hochgeladen oder Datei zu groß!</p>";
				$request['allowUpload']=false;
			}

			//valid thumbnail
			if($_POST['type']=='photo'||getimagesize($_FILES["thumbnail"]["tmp_name"])!==false || $_FILES["thumbnail"]["size"] > $maxThumbnailSize){
				//valid files
				($_POST['type']!='photo'?$request['thumbnail']=true:$request['thumbnail']=false);
			}else{
				$request['allowUpload']=false;
				echo'<script>alert("Bitte gültiges Thumbnail hochladen!");</script>';
			}
		}else{
			$request['allowUpload']=false;
			echo '<script>alert("Bitte Thumbnail und Mediendatei/Videolink hochladen!");</script>';
		}
	}else{
		$request['allowUpload']=false;
	}

	//edit media
	if(isset($_POST['updateMedia'])){
		//get all selected categories
		$categories=[];
		foreach($_POST as $cat=>$val){
			if($val=='on'){
				$categories[]=str_replace('_',' ',$cat);
			}
		}
		unset($cat);
		$request['updateMedia']=['id'=>$_POST['updateMedia'], 'title'=>$_POST['title'],
														'price'=>$_POST['price'],'categories'=>$categories];
		if(isset($_POST['description'])){
			$request['updateMedia']['description']=$_POST['description'];
		}
		if(isset($_POST['creator'])){
			$request['updateMedia']['creator']=$_POST['creator'];
		}
	}
	//delete media
	isset($_POST['deleteMedia'])?$request['deleteMedia']=$_POST['deleteMedia']:null;

	//show only selected photos
	if(isset($_POST['userSelect'])){
		$request['userSelect']=str_replace("'s",'', $_POST['userSelect']);
	}else{
		if(isset($_GET['type']) && $_GET['type']=='photo'){
			$request['userSelect']='deine';
		}else{
			$request['userSelect']='alle';
		}
	}

	//get user permissions from login check
	$request['permissions']=$perms;
	$uploadController=new Controller($request);
	echo $uploadController->display();


 ?>
