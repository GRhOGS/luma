<?php
  class View{

    //stores variables used in the template
    private $_ = array();
    //template name
    private $template = 'default';
    //possible categories in dropdown menu
    private $categories = ['Strom','Licht','Lichtformer','Ton','Kamera','Stativ/Gimbal','Lichtstativ','Objektiv',
                          'Akku','sonstige'];
    private $permissions = 0;

    // set all options for a dropdown menu
    public static function printOptions($options, $default=false){
      //make default option appear first
      if($default!=false){
        echo'"<option value="'.$default.'">'.$default.'</option>';
      }
      //set all options except default
      foreach($options as $option){
        if($option!=$default){
          echo'"<option value="'.$option.'">'.$option.'</option>';
        }
      }
    }

    //get file extension of file
    public static function getExtension($fileName, $path='media/'){
      foreach(scandir($path, SCANDIR_SORT_DESCENDING) as $entry){
        if (substr($entry,0,-4)==$fileName){
          return(".".pathinfo($entry, PATHINFO_EXTENSION));
        }
      }
      return false;
    }

    //assign variables to keys
    public function assign($key, $value){
        $this->_[$key] = $value;
    }

    //set template name
    public function setTemplate($template = 'default'){
       $this->template = $template;
    }

    //set permissions
    public function setPermissions($perms=0){
      $this->permissions = $perms;
    }

    //load and return template file
    public function loadTemplate(){
        $tplName=$this->template;
        // template path
        $file = 'templates' . DIRECTORY_SEPARATOR . $tplName . '.php';

        if (file_exists($file)){
            // save output in buffer
            ob_start();
            include $file;
            $output = ob_get_contents();
            ob_end_clean();

            // return output
            return $output;
        }
        else {
            // error message if template file doesn't exist
            return 'could not find template';
      }
    }
  }
?>
