<?php
require_once('etc/pdo_connect.php'); //pdo connection

class Model extends DBh{

  //check validity of user input and clean it
  public static function cleanInput($newData , $exeptions=[]){
    //differentiate between string and array as input
    if(!is_array($newData)){
      //clean input (string)
      if(empty($newData) && $newData!=0){
        return false;
      } else {
        // trim whitespace
        $cleanup = trim($newData);
        //$cleanup=str_replace('"',"'", $cleanup);    //for db compatibility
      }
    }else{
      //clean input (array)
      foreach($newData as $key => $attribute){
        if(empty($attribute) && $attribute!=0 && !in_array($key, $exeptions)){
          return false;
        } else {
          // trim whitespace and add to array
          if(is_array($attribute)){
            $cleanup[$key] = Model::cleanInput($attribute);
            if($cleanup[$key]===false)return false;
          }else{
            $cleanup[$key] = trim($attribute);
            //$cleanup=str_replace('"',"'", $cleanup);    //for db compatibility
          }
        }
      }
    }
    return $cleanup;
  }

  //validate login credentials and return permission level of user
  //Quelle für Hashtutorial: https://www.php-einfach.de/experte/php-sicherheit/daten-sicher-speichern/php-passwort-sicherheit/
  public function validateLogin($username, $passwort){
    //check for missing data
    $username = self::cleanInput($username);
    $passwort = self::cleanInput($passwort);

    if($username===false||$passwort===false){
        return false;
    }else{
      $pdo = $this->connect();

      //get password and permission level from specific user
      $query = "SELECT mitglied.passwort, rolle.berechtigung FROM mitglied
                JOIN mitgliedtorolle ON mitglied.username=mitgliedtorolle.username
                JOIN rolle ON mitgliedtorolle.funktion=rolle.funktion
                AND mitglied.username=?";
      $stmt=$pdo->prepare($query);
      $stmt->execute([$username]);

      //get highest permission level out of all associated
      $permissions=-1;
      while($row = $stmt->fetch()){
        $dbPass=$row['passwort'];
        $row['berechtigung']>$permissions?$permissions=$row['berechtigung']:null;
      }

      //verify password
      if(isset($dbPass) && password_verify($passwort, $dbPass)){
        //if not already logged in
        if(!isset($_SESSION['username']) && !isset($_SESSION['passwort'])){
          //save username and password in session data
          $_SESSION['username']=$username;
          $_SESSION['passwort']=$passwort;

          //rehash password if needed
          if(password_needs_rehash($dbPass, PASSWORD_DEFAULT)){
            echo"rehashing password";
            $hash = password_hash($passwort, PASSWORD_DEFAULT);
            $query = "UPDATE mitglied SET passwort=? WHERE username=?";
            $stmt=$pdo->prepare($query);
            $stmt->execute([$hash, $username]);
          }
        }

        //return permissions
        return $permissions;
      } else {
        //reject password
        return false;
      }
    }
  }

  //returns all usernames
  public function getUsernames(){
    $pdo = $this->connect();

    $query = "SELECT username FROM mitglied";

    $stmt=$pdo->prepare($query);
    $stmt->execute();

    while($row =  $stmt->fetch()){
      $usernames[]=$row['username'];
    }
    return $usernames;
  }

  //get all unique genres (opt: by media type)
  public function getGenres($type=''){
    $pdo = $this->connect();
    $query = "SELECT DISTINCT genre FROM mediumtogenre
              JOIN medium ON medium.medien_id=mediumtogenre.medien_id";
              if ($type=='video') {
                $query.=" AND medium.medientyp = '2'";
              }
              else if ($type=='musik') {
                $query.=" AND medium.medientyp = '1'";
              }
              else if ($type=='foto') {
                $query.=" AND medium.medientyp = '0'";
              }

    $stmt=$pdo->prepare($query);
    $stmt->execute();

    $genres=[];
    while($row =  $stmt->fetch()){
      $genres[]=$row['genre'];
    }
    return $genres;
  }

  //return all team members with their associated roles (opt: all of one role)
  public function getTeam($role='all'){
    $pdo = $this->connect();
    $query = "SELECT mitglied.username, mitglied.vorname, mitglied.name, rolle.funktion,
                    mitglied.instalink, mitglied.youtubelink, mitglied.beschreibung, mitglied.pos, mitglied.email
              FROM mitglied JOIN mitgliedtorolle ON mitglied.username=mitgliedtorolle.username
              JOIN rolle ON mitgliedtorolle.funktion=rolle.funktion";
              if($role!='all'){
                $query.=" AND rolle.funktion = '".$role."'";
              }
              $query.=" ORDER BY mitglied.pos ASC, rolle.berechtigung DESC";
    $stmt=$pdo->prepare($query);
    $stmt->execute();

    $i = 0;
    // safe data in array and avoid double entries
    while ($row = $stmt->fetch()) {
      if(!isset($output[$row['username']])){
        $output[$row['username']]=['username'=>$row['username'], 'vorname'=>$row['vorname'],
                                  'name'=>$row['name'], 'instalink'=>$row['instalink'],
                                  'youtubelink'=>$row['youtubelink'], 'beschreibung'=>$row['beschreibung'],
                                  'email'=>$row['email'], 'pos'=>$row['pos']];
      }
      $output[$row['username']]['funktion'][]= $row['funktion'];
    }

    return $output;
  }

  //get all roles
  public function getRoles(){
    $pdo = $this->connect();

    //prepare query
    $query = "SELECT funktion, berechtigung FROM rolle ORDER BY berechtigung ASC";
    $stmt=$pdo->prepare($query);
    $stmt->execute();

    //save data for return
    while($row = $stmt->fetch()){
      $output[$row['funktion']]=$row['berechtigung'];
    }
    return $output;
  }

  public function reorderPos($username, $pos){
    //get all usernames and positions
    $pdo = $this->connect();
    $query = 'SELECT username, pos FROM mitglied ORDER BY pos ASC';
    $stmt=$pdo->prepare($query);
    $stmt->execute();
    while($row=$stmt->fetch()){
      if($row['pos']!=0) $positions[$row['pos']]=$row['username'];
    }

    //if something needs to be changed
    $query='UPDATE `mitglied` SET `pos`=? WHERE `username`=?';
    if($pos>count($positions))$pos=count($positions);

    if($pos==0 || $positions[$pos]!=$username){
      //'delete' pos of user that needs to be changed and decrement all higher positions
      for($current=array_search($username, $positions)+1; $current<=count($positions); $current++){
        $stmt=$pdo->prepare($query);
        $stmt->execute([$current-1, $positions[$current]]);
        $positions[$current-1]=$positions[$current];
      }
      unset($positions[count($positions)]);

      if($pos==0){
        $stmt=$pdo->prepare($query);
        $stmt->execute([$pos, $username]);
        return true;
      }

      //move all usernames and insert 'new' pos
      for($current=$pos; $current <= count($positions); $current++){
        $stmt=$pdo->prepare($query);
        $stmt->execute([$current+1, $positions[$current]]);
      }
      $stmt=$pdo->prepare($query);
      $stmt->execute([$pos, $username]);
    }
  }

  public function editMember($updateData){
    //check for missing data
    $updateData = self::cleanInput($updateData,['youtubelink', 'instalink', 'email', 'beschreibung']);

    if($updateData===false){
        return false;
    }else{
      $pdo = $this->connect();
      $query='UPDATE `mitglied` SET `username`=?,`vorname`=?,`name`=?,`email`=?,
                      `youtubelink`=?,`instalink`=?,`beschreibung`=?
              WHERE `username`=?';
      $stmt=$pdo->prepare($query);
      $stmt->execute([$updateData['username'], $updateData['vorname'], $updateData['name'],
                      $updateData['email'], $updateData['youtubelink'], $updateData['instalink'],
                      $updateData['beschreibung'], $updateData['oldNick']]);

      return $this->reorderPos($updateData['username'], $updateData['pos']);
    }
  }

  public function newMember($newData){
    //check for missing data
    $newData = self::cleanInput($newData, ['email', 'youtubelink', 'instalink']);

    if($newData===false){
        return false;
    }else{
      $pdo = $this->connect();
      $query = "INSERT INTO `mitglied` (`username`, `vorname`, `name`, `passwort`,
                                        `email`, `youtubelink`, `instalink`, `pos`,
                                        `beschreibung`)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

      //check query(new member) success
      $stmt=$pdo->prepare($query);
      $stmt->execute([$newData['username'],$newData['vorname'], $newData['name'],
                      password_hash($newData['pass'], PASSWORD_DEFAULT),
                      $newData['email'], $newData['youtubelink'], $newData['instalink'],
                      $newData['pos'], $newData['beschreibung']]);
      if ($stmt->rowCount()==0) {
        return false;   //sql error
      } else {
        //add roles
        foreach($newData['funktion'] as $role){
          $query = 'INSERT INTO mitgliedtorolle (username, funktion)
                    VALUES (?,?)';
          $stmt=$pdo->prepare($query);
          if(!$stmt->execute([$newData['username'],$role])) {
            return false;   //sql error
          }
        }
      }
      if(isset($_FILES)){
        $target_file = "pics/Profile-Pictures/". $newData['username'].'.jpg';
        move_uploaded_file($_FILES['headshot']["tmp_name"], $target_file);
      }
      return true;
    }
  }

  public function deleteMember($username){
    $pdo = $this->connect();
    $usernameQuery = "DELETE mitgliedtorolle FROM mitglied
                      JOIN mitgliedtorolle ON mitglied.username=mitgliedtorolle.username
                      WHERE mitglied.username='".$username."'";
    $equipmentQuery = "DELETE mitgliedtoequipment, equipment, equipmenttoequipmentliste FROM equipment
                      JOIN mitgliedtoequipment ON equipment.equipment_id=mitgliedtoequipment.equipment_id
                      JOIN equipmenttoequipmentliste ON equipment.equipment_id=equipmenttoequipmentliste.equipment_id
                      WHERE equipment.equipment_id='[id]'";
    $mediumQuery = "DELETE mitgliedtomedium, medium, mediumtogenre, mitglied FROM mitglied
                      JOIN mitgliedtomedium ON mitglied.username=mitgliedtomedium.username
                      JOIN medium ON mitgliedtomedium.medien_id=medium.medien_id
                      JOIN mediumtogenre ON medium.medien_id=mediumtogenre.medien_id
                      WHERE mitglied.username='".$username."'";

    $stmt=$pdo->prepare($usernameQuery);
    if (!$stmt->execute()) {
      return false;   //sql error
    }

    //potentially delete equipment
    $query = "SELECT `username`, `equipment_id` FROM `mitgliedtoequipment`
              WHERE username='".$username."'";
    $stmt=$pdo->prepare($query);
    $stmt->execute();
    while($row = $stmt->fetch()){
      $query = "SELECT `username`, `equipment_id` FROM `mitgliedtoequipment`
                WHERE equipment_id='".$row['equipment_id']."'";
      $inner_stmt=$pdo->prepare($query);
      $inner_stmt->execute();
      if($inner_stmt->rowCount()==1){  //if equipment is only linked to account being deleted
        //delete from all tables
        $inner_stmt=$pdo->prepare(str_replace('[id]', $row['equipment_id'], $equipmentQuery));
        if (!$inner_stmt->execute()) {
          return false;   //sql error
        }
      }
    }

    //move files to cloud folder and delete from DB
    $query = "SELECT medium.medien_id, medium.link FROM medium
              JOIN mitgliedtomedium ON medium.medien_id=mitgliedtomedium.medien_id
              WHERE mitgliedtomedium.username='".$username."'";
    $stmt=$pdo->prepare($query);
    $stmt->execute();
    while($row = $stmt->fetch()){
      rename($_SERVER['DOCUMENT_ROOT'].'/'.$row['link'], $_SERVER['DOCUMENT_ROOT'].'/file_storage/old_'. $row['link']);
    }

    $stmt=$pdo->prepare($mediumQuery);
    if(!$stmt->execute()){
      return false; //sql error
    }

    unlink('pics/Profile-Pictures/'. $username.'.jpg');
    return true;
  }

  public function addRoleToMember($newData){
    $pdo = $this->connect();
    $query="INSERT INTO `mitgliedtorolle` (`username`, `funktion`) VALUES (?,?)";
    $stmt=$pdo->prepare($query);
    if (!$stmt->execute([$newData[1], $newData[0]])) {
      return false;   //sql error
    }

    return true;
  }

  //get all media, filtered by genre, count of needed media, mediatype and creator
  public function getMedia($sort, $count, $type='all', $creator='alle'){
    $pdo = $this->connect();

    //prepare query
    if($sort=="Neu zuerst"){
      $query = 'SELECT link, titel, beschreibung, medium.medien_id, preis FROM medium ';
                if($creator!='alle'){
                  $query.= 'JOIN mitgliedtomedium ON medium.medien_id=mitgliedtomedium.medien_id
                            WHERE username="'.($creator=='deine'?$_SESSION['username']:$creator).'"';
                }else{
                  $query.= ' WHERE true';
                }
    }else{
      $query = 'SELECT link, titel, beschreibung, medium.medien_id, preis FROM medium
                JOIN mediumtogenre ON medium.medien_id=mediumtogenre.medien_id
                AND mediumtogenre.genre="'.$sort.'"';
    }

    //select from different media types
    if($type=='video'){
      $query .= ' AND medium.medientyp = "2"';
    }else if($type=='music'||$type=='audio'){
      $query .= ' AND medium.medientyp = "1"';
    }else if($type=='foto'||$type=='photo'){
      $query .= ' AND medium.medientyp = "0"';
    }

    //order and limit query if wanted
    if($sort=='Neu zuerst'){
      $query.=' ORDER BY medium.medien_id DESC';
    }elseif($type=='foto' && $count==3 ||$type=='photo' && $count==3){
      $query.=' ORDER BY RAND()';
    }
    if($count!='alle'){
      $query.=' LIMIT '. $count;
    }

    //execute query
    $stmt=$pdo->prepare($query);
    $stmt->execute();

    // safe data in array
    while($row = $stmt->fetch()){
      $output[$row['medien_id']]=['link'=>$row['link'], 'titel'=>$row['titel'], 'beschreibung'=>$row['beschreibung'], 'preis'=>$row['preis']];
    }
    return isset($output)?$output:false;
  }

  //get media by type plus associated creator & genre
  public function getMediaPlus($type, $creator){
    $pdo = $this->connect();
    //get all media of a type
    $model = new Model();
    $media = $model->getMedia('Neu zuerst', 'alle', $type, $creator);
    unset($model);

    //associate genres and member
    if($media){
      foreach($media as $id=>$medium){
        //get all genre
        $query = 'SELECT genre FROM mediumtogenre WHERE medien_id=?';
        $stmt=$pdo->prepare($query);
        $stmt->execute([$id]);

        while($row = $stmt->fetch()){
          $media[$id]['categories'][]=$row['genre'];
        }

        //get crewmember
        if($type=='photo'){
          $query = 'SELECT mitglied.username FROM mitglied JOIN mitgliedtomedium
                    ON mitgliedtomedium.username=mitglied.username AND medien_id=?';
          $stmt=$pdo->prepare($query);
          $stmt->execute([$id]);
          $media[$id]['creator']= $stmt->fetch()['username'];
        }
      }
      return $media;
    }else{
      return false;
    }
  }

  //get foto by photographer
  public function getFotoByFotograf($sort,$fotograf=''){
    $pdo = $this->connect();

    //prepare query
    $query = "SELECT medium.medien_id, link, name, vorname, instalink, titel FROM medium
              JOIN mitgliedtomedium ON medium.medien_id=mitgliedtomedium.medien_id
              JOIN mitglied ON mitglied.username=mitgliedtomedium.username AND medium.medientyp = '0'";
              if($fotograf!=''){
                $query.=" AND mitglied.username='".$fotograf."'";
              }
              if($sort!='Neu zuerst'){
                $query.=" JOIN mediumtogenre ON medium.medien_id = mediumtogenre.medien_id ";
                $query.=" AND mediumtogenre.genre='".$sort."'";
              }
              $query.=" ORDER BY medium.medien_id DESC";

    $stmt=$pdo->prepare($query);
    $stmt->execute();

    //save data for return
    while($row = $stmt->fetch()){
      $output[]=['medien_id'=>$row['medien_id'],'titel'=>$row['titel'], 'link'=>$row['link'], 'name'=>$row['name'], 'vorname'=>$row['vorname'], 'instalink'=>$row['instalink']];
    }
    return $output;
  }

  //returns all/one equipment and associated owner(s)
  public function getEquipment($id=false, $catSort=''){
      //get login info from DB
      $pdo = $this->connect();
      $query = "SELECT  equipment.equipment_id, equipment.name, equipment.kategorie,
                        mitgliedtoequipment.anzahl, mitglied.username, mitglied.vorname FROM equipment
                JOIN mitgliedtoequipment ON equipment.equipment_id=mitgliedtoequipment.equipment_id
                JOIN mitglied ON mitgliedtoequipment.username=mitglied.username";
                if($id){
                  $query .= " AND equipment.equipment_id='".$id."'";
                }
                if($catSort!='' && $catSort!='alle'){
                  $query .= " AND equipment.kategorie = '".$catSort."'";
                }
                $query .= " ORDER BY equipment.kategorie ASC";

      $stmt=$pdo->prepare($query);
      $stmt->execute();

      //save returned data
      $output=[];
      while($row = $stmt->fetch()){
        // add additional owner to already selected equipment
        if(array_key_exists($row['equipment_id'],$output)){
          $output[$row['equipment_id']]['total'] += $row['anzahl'];
          $output[$row['equipment_id']]['name'][]= $row['vorname'];
          $output[$row['equipment_id']]['username'][]= $row['username'];
          $output[$row['equipment_id']]['count'][]= $row['anzahl'];
        } else {
          //save equipment information in array
          $output[$row['equipment_id']]=['id'=>$row['equipment_id'],'equipmentName'=>$row['name'],'total'=>$row['anzahl'],
                                        'category'=>$row['kategorie'],'name'=>[$row['vorname']],
                                         'username'=>[$row['username']], 'count'=>[$row['anzahl']]];
        }
      }
      return $output;
    }

  //get all equipment lists names + id's
  public function getEquipmentLists(){
    $pdo = $this->connect();
    $query = "SELECT name, listen_id FROM equipmentliste";
    $stmt=$pdo->prepare($query);
    $stmt->execute();
    return $stmt->fetchAll();
  }

  //get a equipment list + all equipment associated
  public function getEquipmentList($id){
    $pdo = $this->connect();

    //prepare query
    $query = "SELECT equipmentliste.name as listenName, equipmentliste.listen_id,
              equipmenttoequipmentliste.anzahl as needed, equipment.equipment_id
              FROM equipmentliste
              JOIN equipmenttoequipmentliste ON equipmenttoequipmentliste.listen_id=equipmentliste.listen_id
                                            AND equipmentliste.listen_id=?
              JOIN equipment ON equipmenttoequipmentliste.equipment_id=equipment.equipment_id";

    $stmt=$pdo->prepare($query);
    $stmt->execute([$id]);
    $row=$stmt->fetch();
    if($row){   //equipment associated
      $model=new Model();
      $output['listID']=$id;
      $output['listName']=$row['listenName'];
      $output['equipment'][]=['equipment'=>$model->getEquipment($row['equipment_id']), 'needed'=>$row['needed'], 'id'=>$row['equipment_id']];
      while($row=$stmt->fetch()){
        //get Owner and equipment data associated with equipment
        array_push($output['equipment'], ['equipment'=>$model->getEquipment($row['equipment_id']), 'needed'=>$row['needed'], 'id'=>$row['equipment_id']]);
      }
      unset($model);
      return($output);
    }else{    //no equipment associated
      $query = "SELECT name FROM equipmentliste WHERE listen_id=?";
      $stmt=$pdo->prepare($query);
      $stmt->execute([$id]);
      $row=$stmt->fetch();
      return array('listName'=>$row['name']);
    }
  }

  //get all equipment ID's from a equipment list
  public function getEquipmentFromList($listID){
    $pdo = $this->connect();
    $query = 'SELECT equipment.equipment_id, equipmenttoequipmentliste.anzahl FROM equipment
              JOIN equipmenttoequipmentliste ON equipmenttoequipmentliste.equipment_id=equipment.equipment_id
              AND equipmenttoequipmentliste.listen_id=?';
    $stmt=$pdo->prepare($query);
    $stmt->execute([$listID]);
    while($row=$stmt->fetch()){
      $output[$row['equipment_id']]=$row['anzahl'];
    }
    if(isset($output)){
      return $output;
    }else{
      return array('0'=>'0');
    }
  }

  //add new equipment
  public function newEquipment($newData){
    //check for missing data
    $newData = self::cleanInput($newData);

    if($newData===false){
        return false;
    }else{
      $pdo = $this->connect();
      $query = "INSERT INTO equipment (equipment_id, name, kategorie)
                VALUES (NULL,?,?)";

      //check query(new equipment) success
      $stmt=$pdo->prepare($query);
      $stmt->execute([$newData['name'],$newData['category']]);
      if ($stmt->rowCount()==0) {
        return false;   //sql error
      } else {
        //get most recent id
        $query = "SELECT equipment_id FROM equipment ORDER BY equipment_id DESC";
        $stmt=$pdo->prepare($query);
        $stmt->execute();
        $id = $stmt->fetch();

        //insert data in table MitgliedToEquipment
        $query = 'INSERT INTO mitgliedtoequipment (username, equipment_id, anzahl)
                  VALUES (?,?,?)';
        $stmt=$pdo->prepare($query);
        if(!$stmt->execute([$newData['username'],$id['0'],$newData['count']])) {
          return false;   //sql error
        } else {
          return true;    //data successfully added
        }
      }
    }
  }

  //add new equipment list
  public function newList($listName){
    //check for missing data
    $listName = self::cleanInput($listName);

    if($listName===false){
        return false;
    }else{
      $pdo = $this->connect();
      $query = "INSERT INTO equipmentliste (listen_id, name) VALUES (NULL, ?)";

      //check query success
      $stmt=$pdo->prepare($query);
      if($stmt->execute([$listName])) {
        $query = "SELECT listen_id FROM equipmentliste ORDER BY listen_id DESC LIMIT 1";
        $stmt=$pdo->prepare($query);
        $stmt->execute();
        return $stmt->fetch()['listen_id'];
      }else{
        return false;
      }
    }
  }

  //update equipment list
  public function updateList($id, $updateData){
    //check for missing data
    $updateData = self::cleanInput($updateData);

    if($updateData===false){
        return false;
    }else{
      $pdo = $this->connect();
      foreach($updateData as $equipID=>$equipCount){
        if($equipCount!=0){
          $query='SELECT anzahl FROM equipmenttoequipmentliste
                  WHERE equipment_id=? AND listen_id=?';
          $stmt=$pdo->prepare($query);
          $stmt->execute([$equipID,$id]);

          //if exists-> update, else->insert new
          if($stmt->fetch()){
            $query = 'UPDATE equipmenttoequipmentliste SET anzahl=?
                      WHERE equipment_id=? AND listen_id=?';
            $stmt=$pdo->prepare($query);
            $stmt->execute([$equipCount,$equipID,$id]);
          }else{
            $query = "INSERT INTO equipmenttoequipmentliste (`equipment_id`, `listen_id`, `anzahl`)
                      VALUES (?,?,?)";
            $stmt=$pdo->prepare($query);
            $stmt->execute([$equipID,$id,$equipCount]);
          }
        }else{
          $query = 'DELETE from equipmenttoequipmentliste
                    WHERE equipment_id=? AND listen_id=?';
          $stmt=$pdo->prepare($query);
          $stmt->execute([$equipID,$id]);
        }
      }
      return true;
    }
  }

  //delete equipment list
  public function deleteList($id){
    $pdo = $this->connect();
    $query = 'DELETE FROM equipmenttoequipmentliste WHERE listen_id=?';
    $stmt=$pdo->prepare($query);
    $stmt->execute([$id]);
    $query = 'DELETE FROM equipmentliste WHERE listen_id=?';
    $stmt=$pdo->prepare($query);
    $stmt->execute([$id]);
  }

  //update equipment in DB
  public function updateEquip($newData){
    //check for missing data
    $newData = self::cleanInput($newData);

    if($newData===false){
        return false;
    }else{
        $pdo = $this->connect();
        $query = 'UPDATE equipment SET name=?, kategorie=? WHERE equipment_id=?';
        $stmt=$pdo->prepare($query);
        $stmt->execute([$newData['name'],$newData['category'],$newData['id']]);

        //check query(new equipment) success
        if ($stmt->rowCount()==0) {
          return false;
        } else {
          return true;
        }
    }
  }

  //delete equip and users, equiplist entries associated
  public function deleteEquip($id){
    $pdo = $this->connect();
    $query='DELETE FROM equipment WHERE equipment_id=?';

    $stmt=$pdo->prepare($query);
    if($stmt->execute([$id])){
      $query='DELETE FROM mitgliedtoequipment WHERE equipment_id=?';
      $stmt=$pdo->prepare($query);
      if($stmt->execute([$id])){
        $query='DELETE FROM equipmenttoequipmentliste WHERE equipment_id=?';
        $stmt=$pdo->prepare($query);
        if($stmt->execute([$id])){
          return true;
        }
      }
    }
    return false;
  }

  //update equipment owners in DB
  public function updateOwners($newData){
    //check for missing data
    $newData = self::cleanInput($newData);

    if($newData===false){
      return false;
    }else{
      $pdo = $this->connect();
      $query = 'SELECT username FROM mitgliedtoequipment WHERE equipment_id=?';
      $stmt=$pdo->prepare($query);
      $stmt->execute([$newData['id']]);
      $names=$stmt->fetchAll();
      $usernames=[];
      foreach($names as $name){
        $usernames[]=$name[0];
      }

      //set query accounting for new username, existing username, count=0
      if(in_array($newData['username'],$usernames)){
        if($newData['count']=="0"){
          $query = "DELETE FROM mitgliedtoequipment";
        }else{
          $query = "UPDATE mitgliedtoequipment SET anzahl='".$newData['count']."'";
        }
          $query .= ' WHERE equipment_id=? AND username=?';
        $stmt=$pdo->prepare($query);
        $stmt->execute([$newData['id'],$newData['username']]);
        return true;
      }else{
        //no new owner with count=0
        if($newData['count']=="0"){
          return false;
        }else{
          $query = 'INSERT INTO mitgliedtoequipment (equipment_id, username, anzahl)
                    VALUES (?,?,?)';
          $stmt=$pdo->prepare($query);
          $stmt->execute([$newData['id'],$newData['username'],$newData['count']]);
          return true;
        }
      }
    }
  }

  //get one or all clients
  public function getClient($id=false){
    //get client(s) info from DB
    $pdo = $this->connect();
    $query = "SELECT  client.client_id as client_id, vorname, name as nachname, email, firma, einzahlung,
              preis, titel, medium.medien_id as medien_id FROM client
              JOIN clienttomedium ON client.client_id=clienttomedium.client_id
              JOIN medium ON medium.medien_id=clienttomedium.medien_id";
              if($id){
                $query .= " AND client.client_id='".$id."'";
              }
              $query .= " ORDER BY firma DESC";

    $stmt=$pdo->prepare($query);
    $stmt->execute();

    //save query output
    $output=[];
    while($row =  $stmt->fetch()){
      if(array_key_exists($row['client_id'],$output)){
        $output[$row['client_id']]['debt']+=$row['preis'];
        $output[$row['client_id']]['projects'][$row['medien_id']]=['title'=>$row['titel'], 'price'=>$row['preis']];
      } else {
        $output[$row['client_id']]=['name'=>$row['vorname']." ".$row['nachname'], 'email'=>$row['email'],
                            'company'=>$row['firma'], 'paid'=>$row['einzahlung'], 'debt'=>$row['preis'],
                            'projects'=>[$row['medien_id']=>['title'=>$row['titel'], 'price'=>$row['preis']]]];
      }
    }
    return $output;
  }

  //add new client
  public function addClient($newData, $seperators){
    //check for missing data
    $newData = self::cleanInput($newData);

    if($newData===false){
        return false;
    }else{
      if(!in_array($newData['project'], $seperators)){
        $pdo = $this->connect();
        //new client data
        $query = "INSERT INTO client (client_id, vorname, name, email, firma)
                  VALUES (NULL, ?,?,?,?)";
        $stmt=$pdo->prepare($query);
        $stmt->execute([$newData['first'],$newData['last'],$newData['email'],$newData['company']]);

        //get ID's
        $query = 'SELECT medien_id FROM medium WHERE titel=?';
        $stmt=$pdo->prepare($query);
        $stmt->execute([$newData['project']]);
        $medID=$stmt->fetch()['medien_id'];
        $query = "SELECT client_id FROM client ORDER BY client_id DESC";
        $stmt=$pdo->prepare($query);
        $stmt->execute();
        $cliID=$stmt->fetch()['client_id'];

        //link client and media
        $query = "INSERT INTO clienttomedium (client_id, medien_id) VALUES (?,?)";
        $stmt=$pdo->prepare($query);
        $stmt->execute([$cliID, $medID]);
        return true;
      }else{
        return false;
      }
    }
  }

  //update client info
  public function updateClient($updateData, $catSeperators){
    //check for missing data
    $updateData = self::cleanInput($updateData);

    if($updateData===false){
        return false;
    }else{
      //update general information
      $pdo = $this->connect();
      $query="UPDATE client SET email=?, einzahlung=? WHERE client_id=?";
      $stmt=$pdo->prepare($query);
      $stmt->execute([$updateData['email'],($updateData['paid']+$updateData['pay']),
                      $updateData['id']]);

      //delete deselected projects
      $query="SELECT medium.medien_id FROM medium
              JOIN clienttomedium ON medium.medien_id=clienttomedium.medien_id
              WHERE client_id=?";
      $stmt=$pdo->prepare($query);
      $stmt->execute([$updateData['id']]);
      while($medium = $stmt->fetch()){
        if(!in_array($medium['medien_id'], $updateData['projects'])){
          $query="DELETE FROM clienttomedium WHERE client_id=? AND medien_id=?";
          $innerSTMT=$pdo->prepare($query);
          $innerSTMT->execute([$updateData['id'],$medium['medien_id']]);
        }
      }

      //add new Project to client
      if(!in_array($updateData['newProject'], $catSeperators)){
        $query = 'SELECT medien_id FROM medium WHERE titel=?';
        $stmt=$pdo->prepare($query);
        $stmt->execute([$updateData['newProject']]);
        $medID=$stmt->fetch();
        $query = "INSERT INTO clienttomedium (client_id, medien_id) VALUES (?,?)";
        $stmt=$pdo->prepare($query);
        $stmt->execute([$updateData['id'],$medID['medien_id']]);
      }
    }
  }

  //delete client and media associated with client
  public function deleteClient($id){
    $pdo = $this->connect();
    $query="DELETE FROM client WHERE client_id=?";
    $stmt=$pdo->prepare($query);
    if($stmt->execute([$id])){
      $query="DELETE FROM clienttomedium WHERE client_id=?";
      $stmt=$pdo->prepare($query);
      if($stmt->execute([$id])){
        //all deletes successfull
        return true;
      }
    }
    //some or all deletes not successfull
    return false;
  }

  //upload media
  public function uploadMedia($newData, $file, $thumb){
    //check for missing data
    $newData = self::cleanInput($newData, ['title']);

    if($newData===false OR (empty($newData['title']) && $newData['type']!='photo')){
        return false;
    }else{
      $pdo = $this->connect();

      //set upload category
      if($newData['type']=='photo'){
        $medientyp=0;
      }else if($newData['type']=='video'){
        $medientyp=2;
        //adjust youtube link to be playable
        if (strpos($newData['link'], 'youtu.be')!== false) {
          $newData['link']=str_replace('https://youtu.be/', 'https://youtube.com/embed/', $newData['link']);
        }elseif((strpos($newData['link'], 'watch?v=')!== false)){
          $newData['link']=str_replace('watch?v=', 'embed/', $newData['link']);
        }
        if (strpos($newData['link'], '&t=')!== false) {
          $newData['link']=substr($newData['link'], 0, strpos($newData['link'], '&t='));
        }

      }else if($newData['type']=='audio'){
        $medientyp=1;
      }else{
        $medientyp=4;
      }

      //new client data
      $newData['description']=isset($newData['description'])?$newData['description']:'';
      $query = "INSERT INTO medium (medien_id, link, beschreibung, preis, titel, medientyp)
                VALUES (NULL, '0',?,?,?,?)";
      $attributes=[$newData['description'],$newData['price'],$newData['title'],$medientyp];
      //create new media
      $stmt=$pdo->prepare($query);
      if($stmt->execute($attributes)){
        //get ID
        $query = 'SELECT medien_id FROM medium ORDER BY medien_id DESC';
        $stmt=$pdo->prepare($query);
        $stmt->execute();
        $ID=$stmt->fetch()['medien_id'];

        //upload files, create path to file
        if($file){
          $target_file = "media/". $ID;
          $target_file .= '.'.pathinfo($_FILES['file']['name'],PATHINFO_EXTENSION);
          move_uploaded_file($_FILES['file']["tmp_name"], $target_file);
          $query="UPDATE medium SET link='". $target_file."' WHERE medien_id=?";
        }else{
          $query="UPDATE medium SET link='". $newData['link']."' WHERE medien_id=?";
        }
        //update link/path to media
        $stmt=$pdo->prepare($query);
        if($stmt->execute([$ID])){
          //upload thumbnail
          if($thumb){
            $target_thumb = "media/thumbnails/". $ID;
            $target_thumb .= '.'.pathinfo($_FILES['thumbnail']['name'],PATHINFO_EXTENSION);
            move_uploaded_file($_FILES['thumbnail']["tmp_name"], $target_thumb);
          }

          //categories
          foreach($newData['categories'] as $category){
            $query="INSERT INTO mediumtogenre (medien_id, genre) VALUES (?,?)";
            $stmt=$pdo->prepare($query);
            $stmt->execute([$ID,$category]);
          }
          //associate member
          if($medientyp==0){
            $query="INSERT INTO mitgliedtomedium (medien_id, username) VALUES (?,?)";
            $stmt=$pdo->prepare($query);
            $stmt->execute([$ID,$newData['creator']]);
          }
        }
        return true;
      }else{
        return false;
      }
    }
  }

  //update media, genre, creator
  public function updateMedia($updateData){
    //check for missing data
    $updateData = self::cleanInput($updateData, ['title']);

    if($updateData===false){
        return false;
    }else{
      $pdo = $this->connect();

      //prepare query
      $query='UPDATE medium SET beschreibung=?, preis=?, titel=? WHERE medien_id=?';

      //prepare values for query
      $updateData['description']=isset($updateData['description'])?$updateData['description']:'';
      $attributes=[$updateData['description'],$updateData['price'],
                   $updateData['title'],$updateData['id']];

      $stmt=$pdo->prepare($query);
      $stmt->execute($attributes);

      //update genres
      $query="DELETE FROM mediumtogenre WHERE medien_id=?";
      $stmt=$pdo->prepare($query);
      if($stmt->execute([$updateData['id']])){
        foreach($updateData['categories'] as $cat){
          $query = 'INSERT INTO mediumtogenre (medien_id, genre) VALUES (?,?)';
          $stmt=$pdo->prepare($query);
          $stmt->execute([$updateData['id'],$cat]);
        }
      }

      //update creator
      if(isset($updateData['creator'])){
        $query="UPDATE mitgliedtomedium SET username=? WHERE medien_id=?";
        $stmt=$pdo->prepare($query);
        $stmt->execute([$updateData['creator'],$updateData['id']]);
      }
    }
  }

  //delete media
  public function deleteMedia($id){
    $pdo = $this->connect();
    //delete media details
    $query = "DELETE FROM medium WHERE medien_id=?";
    $stmt=$pdo->prepare($query);
    $stmt->execute([$id]);
    //delete connection to genre
    $query = "DELETE FROM mediumtogenre WHERE medien_id=?";
    $stmt=$pdo->prepare($query);
    $stmt->execute([$id]);
    //delete conncetion to members
    $query = "DELETE FROM mitgliedtomedium WHERE medien_id=?";
    $stmt=$pdo->prepare($query);
    $stmt->execute([$id]);

    //delete thumbnail
    $path="media/";
    if(View::getExtension($id, $path)){
      unlink($path.$id.View::getExtension($id, $path));
    }

    //delete file
    $path.="thumbnails/";
    if(View::getExtension($id, $path)){
      unlink($path.$id.View::getExtension($id, $path));
    }
  }

  //upload file
  public function uploadCloudFile($newFile){
    setlocale(LC_ALL, 'de_DE@euro', 'de_DE', 'de', 'ge');         //something something compatability idk

    //upload file
    $target_file  = "file_storage/". $newFile['path'];                          //path to file
    $target_file .= pathinfo($_FILES['file']['name'],PATHINFO_FILENAME);        //original filename
    $target_file .= "[".$_SESSION['username']."]";                              //uploader-flag
    $target_file .= $newFile['private']==1?'[prv]':'';                          //private-flag
    $target_file .= '.'.pathinfo($_FILES['file']['name'],PATHINFO_EXTENSION);   //extension
    return move_uploaded_file($_FILES['file']["tmp_name"], $target_file);
  }

  //create new Directory
  public function createFolder($newFolder){
    //check for missing data
    $newFolder = self::cleanInput($newFolder, ['path']);

    if($newFolder===false){
        return false;
    }else{
      return mkdir('file_storage/'.$newFolder['path'].$newFolder['name']);
    }
  }

  //atomize file names
  public static function formatFilenames($unformatted){
    $files=[];
    foreach($unformatted as $file){
      $shortname=$file['file'];

      //check if file is private
      if(strpos($shortname, '[prv]')){
        $private=true;
        $shortname=str_replace('[prv]', '', $shortname);
      }else{
        $private=false;
      }

      //check who created it
      $bracket1=strrpos($shortname, '[');
      $bracket2=strrpos($shortname, ']');
      $owner=substr($shortname, $bracket1+1, $bracket2-$bracket1-1);
      $shortname=str_replace('['.$owner.']', '', $shortname);

      //save data
      $files[]=['name'      =>  pathinfo($shortname,PATHINFO_FILENAME),
                'fullname'  =>  $file['file'],
                'extension' =>  pathinfo($shortname,PATHINFO_EXTENSION),
                'private'   =>  $private,
                'owner'     =>  $owner,
                'path'      =>  str_replace('file_storage/','',$file['path'])];
    }
    return $files;
  }

  //get files from one Directory
  public function getFilesFromDir($path){
    $files=[];
    $cdir = scandir('file_storage/'.$path);
    //only get files, not directories
    foreach ($cdir as $file){
      if (!in_array($file, array(".","..")) && !is_dir('file_storage/'. $path. DIRECTORY_SEPARATOR. $file)){
        $files[]=['file'=>$file,'path'=>$path];
      }
    }
    return self::formatFilenames($files);
  }

  //search for files
  public static function scanFiles($dir,$searchname){
  $files = scandir($dir); //all files in current dir
  $foundfiles=array();
  foreach($files as $file){
    $path = $dir.'/'.$file;
    if(!is_dir($path)) {
      if(strpos($file, $searchname)!==false){
        $foundfiles[]=['file'=>$file, 'path'=>str_replace('file_storage/','',$dir).'/'];
      }
    } else if($file != "." && $file != "..") { //go to next directory
      $foundfiles=array_merge($foundfiles, self::scanFiles($path, $searchname));
    }
   }
   return $foundfiles;
  }

  public function searchFiles($searchname){
    //check for missing data
    $searchname = self::cleanInput($searchname);

    if($searchname===false){
        return 'lucasislow';
    }else{
        $foundfiles=self::scanfiles('file_storage', $searchname);
        if($foundfiles){
          return self::formatFilenames($foundfiles);
        }else{
          return false;
        }
    }
  }
  //download file
  public function downloadFile($downloadFile){
    $file='file_storage/'.$downloadFile['fullname'];
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.
            $downloadFile['shortname'].'.'.pathinfo($file,PATHINFO_EXTENSION).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    readfile($file);
  }

  //delete file
  public function deleteFile($file){
    return unlink('file_storage/'.$file);
  }

  //edit file name/priv. status
  public function editFile($changeDetails){
    //check for missing data
    $changeDetails = self::cleanInput($changeDetails, ['private', 'path']);
    if($changeDetails===false){
      return false;
    }else{
      $oldFile='file_storage/'.$changeDetails['fullname'];
      $newFile='file_storage/'.$changeDetails['path'].$changeDetails['name'].
                '['.$changeDetails['owner'].']'.$changeDetails['private'].'.'.$changeDetails['extension'];
      return rename($oldFile, $newFile);
    }
  }
}
?>
