<?php
	session_start();	//start session for session storage
	include('classes/controller.php');
	include('classes/model.php');
	include('classes/view.php');


	$request=array();
	$request['view']='videos';

	if(isset($_GET['sortVid']) && $_GET['sortVid'] != null){
		$request['sortVid']=$_GET['sortVid'];
	}else{
		$request['sortVid']='Neu zuerst';
	}

	if(isset($_GET['vidCount'])){
		$request['vidCount']=$_GET['vidCount'];
	}else{
		$request['vidCount']=4;
	}

	$videoController=new Controller($request);
	echo $videoController->display();

 ?>
